package com.example.inlaemning2;

import com.example.inlaemning2.game.GameEntity;
import com.example.inlaemning2.game.StatusResponseDTO;
import com.example.inlaemning2.player.PlayerEntity;
import com.example.inlaemning2.player.PlayerResponseDTO;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/games")
public class GameController {

    GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/games")
    public List<GameEntity> gameList() {
        return gameService.gameList();
    }

    @PostMapping("/start/{playerId}")
    public StatusResponseDTO startGame(@PathVariable("playerId") String playerId) {
        return toDTO(
                gameService.startGame(playerId));

    }

    @GetMapping("/join/{id}/{playerId}")
    public StatusResponseDTO joinGame(@PathVariable("id") String id, @PathVariable("playerId") String playerId) {
        return toDTO(
                gameService.joinGame(id,
                        playerId));
    }

    @GetMapping("/{id}/{playerId}")
    public StatusResponseDTO getStatus(@PathVariable("id") String id, @PathVariable("playerId") String playerId) {
        return toDTO(
                gameService.getStatus(id, playerId));
    }

    @PostMapping("/move/{id}/{playerId}/{move}")
    public StatusResponseDTO makeMove(
            @PathVariable("id") String id,
            @PathVariable("playerId") String playerId,
            @PathVariable("move") String move
    ) {
        return toDTO(
                gameService.makeMove(id, playerId, move)
        );
    }

    private static StatusResponseDTO toDTO(GameEntity gameEntity) {
        return new StatusResponseDTO(
                gameEntity.getId(),
                gameEntity.getPlayer1Name(),
                gameEntity.getPlayer1Move(),
                gameEntity.getStatus(),
                gameEntity.getPlayer2Name(),
                gameEntity.getPlayer2Move()
        );
    }
}
