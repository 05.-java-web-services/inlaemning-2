package com.example.inlaemning2;

import com.example.inlaemning2.game.*;
import com.example.inlaemning2.player.PlayerEntity;
import com.example.inlaemning2.player.PlayerRepository;
import org.springframework.data.repository.query.parser.Part;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class GameService {
    GameRepository gameRepository;
    PlayerRepository playerRepository;

    public GameService(GameRepository gameRepository, PlayerRepository playerRepository) {
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
    }

    public List<GameEntity> gameList() {
        return gameRepository.findAll();
    }

    //Startar ett nytt spel, och tar namnet från Player-repo för att sätta dit namn
    public GameEntity startGame(String playerId) {
        PlayerEntity playerEntity = getPlayer(playerId);
        GameEntity gameEntity = new GameEntity();

        gameEntity.setPlayer1Name(playerEntity.getName());
        gameEntity.setStatus(String.valueOf(Status.OPEN));
        return gameRepository.save(gameEntity);
    }

    /* Tar namnet på player1 och sätter den till player2, så att player som
    går med i spelet kan ha sitt namn på player 1-platsen */
    public GameEntity joinGame(String id, String playerId) {
        PlayerEntity playerEntity = getPlayer(playerId);
        GameEntity gameEntity = getGame(id);

        gameEntity.setPlayer2Name(gameEntity.getPlayer1Name());

        gameEntity.setPlayer1Name(playerEntity.getName());
        gameEntity.setStatus(String.valueOf(Status.ACTIVE));

        return gameRepository.save(gameEntity);
    }

    public GameEntity makeMove(String id, String playerId, String move) {
        PlayerEntity playerEntity = getPlayer(playerId);
        GameEntity gameEntity = getGame(id);

        playerEntity.setMove(move);
        playerRepository.save(playerEntity);

        gameEntity.setPlayer2Name(gameEntity.getPlayer1Name());
        gameEntity.setPlayer2Move(gameEntity.getPlayer1Move());

        gameEntity.setPlayer1Name(playerEntity.getName());
        gameEntity.setPlayer1Move(move);

        return gameRepository.save(gameEntity);
    }

    //Hämtar status och visar namnet på anroparen på player 1-platsen
    public GameEntity getStatus(String id, String playerId) {
        PlayerEntity playerEntity = getPlayer(playerId);
        GameEntity gameEntity = getGame(id);

        gameEntity.setPlayer2Name(gameEntity.getPlayer1Name());
        gameEntity.setPlayer2Move(gameEntity.getPlayer1Move());

        gameEntity.setPlayer1Name(playerEntity.getName());
        gameEntity.setPlayer1Move(playerEntity.getMove());

        gameRepository.save(gameEntity);

        if(gameEntity.getPlayer2Move() == null) {
            gameEntity.setStatus(String.valueOf(Status.ACTIVE));
        } else if(gameEntity.getPlayer1Move().equals("rock")) {
            if (gameEntity.getPlayer2Move().equals("scissors")) {
                gameEntity.setStatus(String.valueOf(Status.WIN));
            } else if (gameEntity.getPlayer2Move().equals("paper")) {
                gameEntity.setStatus(String.valueOf(Status.LOSE));
            } else if (gameEntity.getPlayer2Move().equals("rock")) {
                gameEntity.setStatus(String.valueOf(Status.DRAW));
            }
        } else if(gameEntity.getPlayer1Move().equals("paper")) {
            if (gameEntity.getPlayer2Move().equals("rock")) {
                gameEntity.setStatus(String.valueOf(Status.WIN));
            } else if (gameEntity.getPlayer2Move().equals("scissors")) {
                gameEntity.setStatus(String.valueOf(Status.LOSE));
            } else if (gameEntity.getPlayer2Move().equals("paper")) {
                gameEntity.setStatus(String.valueOf(Status.DRAW));
            }
        } else if(gameEntity.getPlayer1Move().equals("scissors")) {
            if (gameEntity.getPlayer2Move().equals("paper")) {
                gameEntity.setStatus(String.valueOf(Status.WIN));
            } else if (gameEntity.getPlayer2Move().equals("rock")) {
                gameEntity.setStatus(String.valueOf(Status.LOSE));
            } else if (gameEntity.getPlayer2Move().equals("scissors")) {
                gameEntity.setStatus(String.valueOf(Status.DRAW));
            }
        }

        return gameRepository.save(gameEntity);
    }

    //Metod för att hämta information om spelaren baserat på spelare-id
    public PlayerEntity getPlayer(String playerId) {
        Optional<PlayerEntity> optionalPlayerEntity = playerRepository.findById(playerId);
        PlayerEntity playerEntity = new PlayerEntity();
        if(optionalPlayerEntity.isPresent()) {
            playerEntity = optionalPlayerEntity.get();
        }
        return playerEntity;
    }

    //Metod för att hämta information om spelet baserat på spel-id
    public GameEntity getGame(String id) {
        Optional<GameEntity> optionalGameEntity = gameRepository.findById(id);
        GameEntity gameEntity = new GameEntity();
        if(optionalGameEntity.isPresent()) {
            gameEntity = optionalGameEntity.get();
        }
        return gameEntity;
    }
}
