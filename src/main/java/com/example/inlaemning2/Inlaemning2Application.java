package com.example.inlaemning2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Inlaemning2Application {

	public static void main(String[] args) {
		SpringApplication.run(Inlaemning2Application.class, args);
	}

}
