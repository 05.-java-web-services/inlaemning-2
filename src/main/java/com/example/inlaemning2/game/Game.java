package com.example.inlaemning2.game;

import lombok.Value;

@Value
public class Game {
    String id;
    String player1Name;
    String player1Move;
    String status;
    String player2Name;
    String player2Move;
}
