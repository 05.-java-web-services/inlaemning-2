package com.example.inlaemning2.game;

import com.example.inlaemning2.player.PlayerEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

import static jakarta.persistence.GenerationType.SEQUENCE;

@Entity(name = "game")
@Table(name = "game")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)

    private String id;

    @Column(name = "player1Name")
    private String player1Name;
    @Column(name = "player1Move")
    private String player1Move;
    @Column(name = "status")
    private String status;
    @Column(name = "player2Name")
    private String player2Name;
    @Column(name = "player2Move")
    private String player2Move;

    /*@OneToMany(mappedBy = "player1")
    @JsonIgnoreProperties("player1")
    private Set<PlayerEntity> player1;

    @OneToMany(mappedBy = "player2")
    @JsonIgnoreProperties("player2")
    private Set<PlayerEntity> player2;*/

    public GameEntity(String name) {
        this.player1Name = name;
    }

}
