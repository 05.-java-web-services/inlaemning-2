package com.example.inlaemning2.game;

import lombok.Value;

@Value
public class StatusResponseDTO {
    String id;
    String name;
    String move;
    String gameInfo;
    String name2;
    String move2;
}
