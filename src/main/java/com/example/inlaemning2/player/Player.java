package com.example.inlaemning2.player;

import lombok.Value;

@Value
public class Player {
    String id;
    String name;
    String move;
}
