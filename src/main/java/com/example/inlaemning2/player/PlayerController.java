package com.example.inlaemning2.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class PlayerController {

    PlayerService playerService;

    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    //Skapar en ny tom spelare, med endast ett UUID
    @PostMapping
    public void createPlayer() {
        playerService.createPlayer();
    }

    @PutMapping("{id}")
    public PlayerResponseDTO setName(@PathVariable("id") String id, @RequestBody PlayerEntity playerEntity) {
        return toDTO(
                playerService.setName(
                        id,
                        playerEntity.getName())
        );
    }

    private static PlayerResponseDTO toDTO(PlayerEntity playerEntity) {
        return new PlayerResponseDTO(
                playerEntity.getId(),
                playerEntity.getName(),
                playerEntity.getMove()
        );
    }
}
