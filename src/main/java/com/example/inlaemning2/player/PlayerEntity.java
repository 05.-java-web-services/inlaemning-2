package com.example.inlaemning2.player;

import com.example.inlaemning2.game.Game;
import com.example.inlaemning2.game.GameEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "player")
@Table(name = "player")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlayerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "move")
    private String move;

    public PlayerEntity(String name) {
        this.name = name;
    }

    /*@ManyToOne
    @JoinColumn(name = "player1")
    private GameEntity player1;

    @ManyToOne
    @JoinColumn(name = "player2")
    private GameEntity player2;*/
}
