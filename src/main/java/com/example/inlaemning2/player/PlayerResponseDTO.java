package com.example.inlaemning2.player;

import lombok.Value;

@Value
public class PlayerResponseDTO {
    String id;
    String name;
    String Move;
}
