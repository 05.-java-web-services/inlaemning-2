package com.example.inlaemning2.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PlayerService {
    PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    //Skapar en ny tom spelare, med endast ett UUID
    public void createPlayer() {
        PlayerEntity playerEntity = new PlayerEntity("Player");
        playerRepository.save(playerEntity);
    }

    public PlayerEntity setName(String id, String name) {
        Optional<PlayerEntity> optionalPlayerEntity = playerRepository.findById(id);
        PlayerEntity playerEntity = new PlayerEntity();
        if(optionalPlayerEntity.isPresent()) {
            playerEntity = optionalPlayerEntity.get();
            playerEntity.setName(name);
        }
        return playerRepository.save(playerEntity);
    }
}
